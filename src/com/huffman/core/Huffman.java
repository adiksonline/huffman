package com.huffman.core;

import java.util.HashMap;
import java.util.Map;

import com.data.struct.PriorityQueue;
import com.data.struct.Stack;
import com.data.struct.Tree;
import com.data.struct.Tree.Node;

public class Huffman {
	private Tree huffmanTree;
	private Map<String, String> lookupMap;
	private String transString = "";
	private PriorityQueue queue;
	
	public Huffman(){
		queue = new PriorityQueue();
	}
	
	public Huffman(String msg){
		setTransString(msg);
	}

	public void setTransString(String msg) {
		transString = msg;
		generateQueue();
		huffmanTree = generateTree();
		huffmanTree.displayTree();
		getCodeMap();
		
	}
	
	public String encode(String msg){
		if (transString.compareTo(msg) != 0)
			setTransString(msg);
		String coded = "";
		for (int i = 0; i < msg.length(); i++)
			coded += lookupMap.get(String.valueOf(msg.charAt(i)));
		return coded;
	}
	
	public String decode(String coded){
		String msg = "";
		Node node = huffmanTree.getRoot();
		for (int i = 0; i < coded.length(); i++){
			switch (coded.charAt(i)){
			case '0':
				node = node.getLeft();
				break;
			case '1':
				node = node.getRight();
				break;
			}
			if (node.isLeaf()){
				msg += node.getName();
				node = huffmanTree.getRoot();
			}
		}
		return msg;
		
	}

	private void getCodeMap() {
		lookupMap = new HashMap<String, String>();
		getLeaves(huffmanTree.getRoot(), new Stack<Integer>(100));
	}

	private void getLeaves(Node node, Stack<Integer> stack) {
		if (node.isLeaf()){
			if (! stack.isEmpty()){
				System.out.printf("Character: %s, Code: %s, Freq: %d\n", node.getName(), stack.toString(), node.getValue());
				lookupMap.put(String.valueOf(node.getName()), stack.toString());
				stack.pop();
			}
			return;
		}
		stack.push(0);
		getLeaves(node.getLeft(), stack);
		stack.push(1);
		getLeaves(node.getRight(), stack);
		if (! stack.isEmpty())
			stack.pop();
	}

	private Tree generateTree() {
		while (queue.getSize() > 1){
			Tree tree = new Tree();
			Node a = queue.pop().getRoot();
			Node b = queue.pop().getRoot();
			Node root = tree.new Node(a.getValue() + b.getValue());
			root.setLeft(a);
			root.setRight(b);
			tree.setRoot(root);
			queue.insert(tree);
		}
		return queue.pop();
	}

	private void generateQueue() {
		String x;
		Map<String, Integer> freqMap = new HashMap<String, Integer>();
		for (int i = 0; i < transString.length(); i++){
			x = String.valueOf(transString.charAt(i));
			if (freqMap.containsKey(x))
				freqMap.put(x, freqMap.get(x) + 1);
			else
				freqMap.put(x, 1);
		}
		for (String s : freqMap.keySet()){
			Tree tree = new Tree();
			Node rootNode = tree.new Node(s.charAt(0), freqMap.get(s));
			tree.setRoot(rootNode);
			queue.insert(tree);
		}
		
	}

}

package com.main;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import com.huffman.core.Huffman;

public class MyApp {

	/**
	 * @param args
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException {
		Huffman huffman = new Huffman();
		System.out.print("Enter the string to encode: ");
		String msg = getString();
		//huffman.setTransString(msg);
		String coded = huffman.encode(msg);
		System.out.print("Encoded string: ");
		System.out.println(coded);
		System.out.printf("Encoded size: %.3f\n", coded.length() / 8.0);
		System.out.print("Decoded string: ");
		String decoded = huffman.decode(coded);
		System.out.println(decoded);
		System.out.println("Decoded size: " + decoded.length());
		double percent = (coded.length() ) * 100.0 / (msg.length() * 8);
		System.out.printf("Encoding ratio: %.3f percent of original\n", percent);

	}

	private static String getString() throws IOException {
		InputStreamReader in = new InputStreamReader(System.in);
		BufferedReader br = new BufferedReader(in);
		String s = br.readLine();
		return s;
	}

}

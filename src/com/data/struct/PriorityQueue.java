package com.data.struct;

public class PriorityQueue {
	Node firstNode;
	int size;
	
	public void insert(Tree tree){
		Node node = new Node(tree);
		if (firstNode == null){
			firstNode = node;
		} else {
			Node current = firstNode;
			Node previous = null;
			while ((current != null) && (current.getTree().compareTo(node.getTree()) < 0)){
				previous = current;
				current = current.getNext();
			}
			if (previous == null)
				firstNode = node;
			else
				previous.setNext(node);
			node.setNext(current);
		}
		size++;
	}
	
	public Tree pop(){
		Node node = firstNode;
		firstNode = firstNode.getNext();
		size--;
		return node.getTree();
	}
	
	public Tree peek(){
		return firstNode.getTree();
	}
	
	public boolean isEmpty(){
		return (firstNode == null);
	}
	
	public int getSize(){
		return size;
	}

	public class Node{
		private Node nextNode;
		private Tree tree;
		
		public Node(Tree t){
			tree = t;
		}
		
		public Tree getTree(){
			return tree;
		}
		
		public Node getNext(){
			return nextNode;
		}
		
		public void setNext(Node n){
			nextNode = n;
		}
	}
}

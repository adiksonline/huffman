package com.data.struct;

public class Tree implements Comparable<Tree>{
	Node rootNode;
	
	public Node getRoot(){
		return rootNode;
	}
	
	public void setRoot(Node n){
		rootNode = n;
	}
	
	public int getValue(){
		if (rootNode == null){
			return 0;
		} else {
			return rootNode.getValue();
		}
	}
	
	public int compareTo(Tree t){
		int comp = new Integer(getValue()).compareTo(new Integer(t.getValue()));
		return comp;
	}
	
	public void displayTree(){
		int space = 64;
		Stack<Node> globalStack = new Stack<Node>(10000);
		Stack<Node> localStack = new Stack<Node>(10000);
		globalStack.push(rootNode);
		boolean doNext = true;
		while (doNext ==true){
			doNext = false;
			for (int i = 0; i < (space / 2); i++)
				System.out.print(" ");
			while (! globalStack.isEmpty()){
				Node item = globalStack.pop();
				if (item == null){
					localStack.push(null);
					localStack.push(null);
					System.out.printf("%s", "*");
				} else {
					if ((item.getLeft() != null) || (item.getRight() != null))
						doNext = true;
					localStack.push(item.getLeft());
					localStack.push(item.getRight());
					if (item.getName() != 0)
						System.out.printf("%s", item.getName());
					else
						System.out.printf("%s", "`");//item.getValue());
				}
				for (int i = 0; i < space - 1; i++)
					System.out.print(" ");
			}
			while (! localStack.isEmpty())
				globalStack.push(localStack.pop());
			System.out.println();
			space /= 2;
		}
		System.out.println();
	}

	public class Node{
		Node leftNode, rightNode;
		int value;
		char name;
		
		public Node(int v){
			value = v;
		}
		
		public Node(char c, int v){
			name = c;
			value = v;
		}
		
		public int getValue(){
			return value;
		}
		
		public char getName(){
			return name;
		}
		
		public boolean isLeaf(){
			return ((leftNode == null) && (rightNode == null));
		}
		
		public void setLeft(Node n){
			leftNode = n;
		}
		
		public void setRight(Node n){
			rightNode = n;
		}
		
		public Node getLeft(){
			return leftNode;
		}
		
		public Node getRight(){
			return rightNode;
		}
	}
}
